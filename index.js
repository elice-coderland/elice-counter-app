const subBtn = document.getElementById('subBtn');
const addBtn = document.querySelector('#addBtn');
const countNum = document.querySelector('#count');

subBtn.addEventListener('click', () => {
  countNum.innerText = parseInt(countNum.innerText) - 1;
});

addBtn.addEventListener('click', () => {
  countNum.innerText = Number(countNum.innerText) + 1;
})